/*
 * Taghreed Safaryan
 *  991494905
 * SYST10199 - Web Programming
 */
package shapes;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author donmi
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({shapes.RectangleTest.class, shapes.shapesDemoTest.class, shapes.SquareTest.class})
public class ShapesSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
