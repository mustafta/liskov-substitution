
package shapes;
import static org.junit.Assert.assertEquals;
public class shapesDemo {
    public static void calculateArea(Rectangle r){
        r.setWidth(2);
        r.setHeight(3);
        assertEquals("Area calculation is incorrect", 6, r.getArea());
    }
    public static void main(String[] args) {
        shapesDemo.calculateArea (new Rectangle());
        shapesDemo.calculateArea (new Square());
    }
}
    
